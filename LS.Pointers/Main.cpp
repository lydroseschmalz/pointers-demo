#include <conio.h>
#include <iostream>

using namespace std;

struct Student
{
	string name;
	float gpa;
};

void SetPerfectGPA(Student * pStud)
{
	pStud->gpa = 4.0;
}

int main() 
{
	//Pointer == variable that points at something
	//if there is a *, look to the left. If there's a data type to the left, you're creating a pointer.

	int i = 6;
	cout << "The value of i is: " << i << "\n";
	cout << "The address of i is: " << &i << "\n"; // here & is "address of"

	int *pI; //pI is a pointer that holds the address of an int
	pI = &i; // setting pI = the address of i

	//declare and assign in one line
	//int *pI = &i;

	//int j, k, l; // 3 ints.
	//int *pJ, pK, pL; // only pJ is the pointer, pK and pL are not. For all three, it's *pJ, *pK, *pL.

	cout << "The value of i is: " << pI << "\n";
	cout << "The value that pI points to is: " << *pI << "\n";




	//Do you want to create a student, then only create enough memory if they actually allocate it:
	Student s; // create student on stack
	SetPerfectGPA(&s);
	Student *pStudent = nullptr;

	char input = 'n';

	cout << "Create student? (y/n):";
	cin >> input;

	if (input == 'y' || input == 'Y') 
	{
		pStudent = new Student; // create a student on the heap
	}

	if (pStudent) 
	{
		//(*pStudent).name = "Lydia"; // this works but usually we use the "->"
		pStudent->name = "Lydia"; // -> is "dereference and access member"
		SetPerfectGPA(pStudent);
	}

	// did other stuff...

	if (pStudent) delete pStudent;



	_getch();
	return 0;
}